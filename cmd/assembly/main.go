package main

import (
	"fmt"

	v1 "gitlab.com/fisherprime/go-tests/internal/assembly"
)

func main() {
	fmt.Println("multiply 64-bit 10*5: ", v1.Multiply64(int64(10), int64(5)))
	fmt.Println("multiply 64-bit & const 10*const: ", v1.Multiply64Const(int64(10)))
	fmt.Println("multiply 32-bit 10*5: ", v1.Multiply32(int32(10), int32(5)))

	v1.HelloWorld()
}
