// SPDX-License-Identifier: MIT
package assembly

import "testing"

func TestMultiply64Const(t *testing.T) {
	type args struct {
		num int64
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{"x10", args{10}, 70},
		{"x12", args{12}, 84},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Multiply64Const(tt.args.num); got != tt.want {
				t.Errorf("Multiply64Const() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMultiply64(t *testing.T) {
	type args struct {
		num1 int64
		num2 int64
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{"5*2", args{5, 2}, 10},
		{"4*15", args{4, 15}, 60},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Multiply64(tt.args.num1, tt.args.num2); got != tt.want {
				t.Errorf("Multiply64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMultiply32(t *testing.T) {
	type args struct {
		num1 int32
		num2 int32
	}
	tests := []struct {
		name string
		args args
		want int32
	}{
		{"5*2", args{5, 2}, 10},
		{"4*15", args{4, 15}, 60},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Multiply32(tt.args.num1, tt.args.num2); got != tt.want {
				t.Errorf("Multiply32() = %v, want %v", got, tt.want)
			}
		})
	}
}
